import logging
import string
import sys

import nltk
import numpy as np
from gensim.models import Word2Vec

sys.path.append('../common/')
import preprocessing

def loadDataToTraining(pharsesNumberLimit=0, encoderSequenceMaxLength=0, decoderSequenceMaxLength=0):
    enEmbeddingsModel = Word2Vec.load('../models/word2vec_pairs_en.model')
    frEmbeddingsModel = Word2Vec.load('../models/word2vec_pairs_fr.model')

    enDatasetName = '../dataset/sentence_pairs/ang.txt'
    frDatasetName = '../dataset/sentence_pairs/fr.txt'

    with open(enDatasetName) as f:
        phrases = f.readlines()[0:pharsesNumberLimit]
    preprocessedData = preprocessing.preprocessEnglish(phrases)
    del phrases
    logging.info('End loading of english sentences')

    enWordsIndex = np.zeros((
        pharsesNumberLimit,
        decoderSequenceMaxLength),
        dtype='float32')
    enOneHotWords = np.zeros(
        (pharsesNumberLimit,
         decoderSequenceMaxLength,
         len(enEmbeddingsModel.wv.vocab))
        , dtype='float32')
    for i, phrase in enumerate(preprocessedData):
        for j, word in enumerate(phrase):
            enWordsIndex[i, j] = enEmbeddingsModel.wv.vocab[word].index
            if j > 0:
                enOneHotWords[i, j - 1, enEmbeddingsModel.wv.vocab[word].index] = 1

    logging.info('End preparing of english sentences')
    del preprocessedData

    with open(frDatasetName) as f:
        phrases = f.readlines()[0:pharsesNumberLimit]
    preprocessedData = preprocessing.preprocessFrench(phrases)
    del phrases
    logging.info('End loading of french sentences')

    frWordsIndex = np.zeros((pharsesNumberLimit, encoderSequenceMaxLength),
                            dtype='float32')
    for i, phrase in enumerate(preprocessedData):
        for j, word in enumerate(phrase):
            frWordsIndex[i, j] = frEmbeddingsModel.wv.vocab[word].index

    logging.info('End preparing of french sentences')
    del preprocessedData

    return enEmbeddingsModel, enWordsIndex, frEmbeddingsModel, frWordsIndex, enOneHotWords
