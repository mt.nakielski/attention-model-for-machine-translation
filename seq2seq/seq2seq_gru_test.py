import logging
import sys
import time

import load_data_pairs as loadData
import numpy as np
import nltk
from keras.models import Model
from keras.layers import Input, GRU, Dense, Embedding, Dropout
from keras.utils import plot_model as plotModel

sys.path.append('../common/')
import draw_data

PHRASES_NUMBER = 1

MAX_SENTENCE_LENGTH_ENCODER = 15
MAX_SENTENCE_LENGTH_DECODER = 10

EMBEDDING_SIZE = 64
GRU_SIZE = 96

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
logging.info('-------------START SEQ2SEQ_GRU--------------')

enWordEmbeddingModel, \
enWordsIndex, \
frWordEmbeddingModel, \
frWordsIndex, \
enOneHotWords = loadData.loadDataToTraining(
    PHRASES_NUMBER,
    MAX_SENTENCE_LENGTH_ENCODER,
    MAX_SENTENCE_LENGTH_DECODER)

logging.info('Start defining model')
encoderInput = Input(shape=(None,), name='wejscie_enkodera')
decoderInput = Input(shape=(None,), name='wejscie_dekodera')

encoderEmbeddingsModel = Embedding(
    len(frWordEmbeddingModel.wv.vectors),
    EMBEDDING_SIZE,
    weights=[frWordEmbeddingModel.wv.vectors],
    trainable=False,
    name='word_Embeddings_enkodera'
)(encoderInput)

encoder = GRU(GRU_SIZE, return_state=True, return_sequences=True, name='jednostka_gru_enkodera')
encoderOutputSequence, hState = encoder(encoderEmbeddingsModel)
encoder_states = [hState]

decoderEmbeddingsModel = Embedding(
    len(enWordEmbeddingModel.wv.vectors),
    EMBEDDING_SIZE,
    weights=[enWordEmbeddingModel.wv.vectors],
    trainable=False,
    name='word_embeddings_dekodera'
)(decoderInput)

decoderGRUUnit = GRU(GRU_SIZE, return_sequences=True, return_state=True, name='jednostka_gru_dekodera')
decoderOutputSequenceElement, _ = decoderGRUUnit(decoderEmbeddingsModel,
                                                     initial_state=encoder_states)

decoderOutputLayer = Dense(len(enWordEmbeddingModel.wv.vectors), activation='softmax',
                           name='wartsta_wyjsciowa_dekodera')
decoderOutputSequenceElement = decoderOutputLayer(decoderOutputSequenceElement)

model = Model([encoderInput, decoderInput], decoderOutputSequenceElement)

model.load_weights('../models/seq2seq_gru.weights')

decoderHStateInput = Input(shape=(GRU_SIZE,))
decoderStatesInput = [decoderHStateInput]
decoderOutputSequenceElement, hState = decoderGRUUnit(
    decoderEmbeddingsModel, initial_state=decoderStatesInput)
decoder_states = [hState]
decoderOutputSequenceElement = decoderOutputLayer(decoderOutputSequenceElement)

encoderModel = Model(encoderInput, encoder_states)
decoderModel = Model(
    [decoderInput] + decoderStatesInput,
    [decoderOutputSequenceElement] + decoder_states)


def translate(inputText):
    inputTextTokens = nltk.word_tokenize(inputText)
    inputSequence = np.zeros((1, MAX_SENTENCE_LENGTH_ENCODER), dtype=np.float32)
    for i, token in enumerate(inputTextTokens):
        inputSequence[0, i] = frWordEmbeddingModel.wv.vocab[token].index
    hState = encoderModel.predict(inputSequence)
    resultSequence = np.zeros((1, 1))
    resultSequence[0, 0] = enWordEmbeddingModel.wv.vocab['<BEGIN> '].index
    stopGenerating = False
    decodedSequence = ''
    currentLength = 0
    while not stopGenerating:
        currentLength = currentLength + 1
        outputOneHotDecodedWord, h = decoderModel.predict([resultSequence] + [hState])
        wordIndex = np.argmax(outputOneHotDecodedWord)
        word = enWordEmbeddingModel.wv.index2word[wordIndex]
        decodedSequence = decodedSequence + word + ' '
        if currentLength > MAX_SENTENCE_LENGTH_DECODER or wordIndex == 1:
            stopGenerating = True
        [hState] = [h]
        resultSequence = [wordIndex]
    return decodedSequence


print('sentence:' + 'je suis trop lent')
print('translation: ' + translate('je suis trop lent') + '\n')

print('sentence:' + 'J\'en ai peur')
print('translation: ' + translate('jen ai peur') + '\n')

print('sentence:' + 'donne moi ça plus tard')
print('translation: ' + translate('donne moi ça plus tard') + '\n')

print('sentence:' + 'j\'ai besoin de ton livre maintenant')
print('translation: ' + translate('jai besoin de ton livre maintenant') + '\n')

print('sentence:' + 'ma femme est vraiment belle')
print('translation: ' + translate('ma femme est vraiment belle') + '\n')

print('sentence:' + 'si je suis rapide, je ne serai pas en retard')
print('translation: ' + translate('si je suis rapide je ne serai pas en retard') + '\n')

print('sentence:' + 'ce livre est trop difficile à comprendre')
print('translation: ' + translate('ce livre est trop difficile à comprendre') + '\n')

print('sentence:' + 'sce plat est trop cher')
print('translation: ' + translate('ce plat est trop cher') + '\n')

print('sentence:' + 'votre enfant est si grand')
print('translation: ' + translate('votre enfant est si grand') + '\n')

print('sentence:' + 'Je me souviens de toi comme d\'un enfant')
print('translation: ' + translate('je me souviens de lui comme dun enfant') + '\n')
