import logging
import sys
import time

import load_data_pairs as loadData
import numpy as np
import nltk
from keras.models import Model
from keras.layers import Input, LSTM, Dense, Embedding, Dropout, dot, Activation, concatenate
from keras.utils import plot_model as plotModel

sys.path.append('../common/')
import draw_data

PHRASES_NUMBER = 80000
EPOCHS = 60

MAX_SENTENCE_LENGTH_ENCODER = 15
MAX_SENTENCE_LENGTH_DECODER = 10

EMBEDDING_SIZE = 64
LSTM_SIZE = 96

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
logging.info('-------------START SEQ2SEQ_LSTM--------------')

enWordEmbeddingModel, \
enWordsIndex, \
frWordEmbeddingModel, \
frWordsIndex, \
enOneHotWords = loadData.loadDataToTraining(
    PHRASES_NUMBER,
    MAX_SENTENCE_LENGTH_ENCODER,
    MAX_SENTENCE_LENGTH_DECODER)

logging.info('Start defining model')
encoderInput = Input(shape=(None,), name='wejscie_enkodera')
decoderInput = Input(shape=(None,), name='wejscie_dekodera')

encoderEmbeddingsModel = Embedding(
    len(frWordEmbeddingModel.wv.vectors),
    EMBEDDING_SIZE,
    weights=[frWordEmbeddingModel.wv.vectors],
    trainable=False,
    name='word_Embeddings_enkodera'
)(encoderInput)

encoder = LSTM(LSTM_SIZE, return_state=True, return_sequences=True, name='jednostka_lstm_enkodera')
encoderOutputSequence, hState, cState = encoder(encoderEmbeddingsModel)
encoder_states = [hState, cState, encoderOutputSequence]

decoderEmbeddingsModel = Embedding(
    len(enWordEmbeddingModel.wv.vectors),
    EMBEDDING_SIZE,
    weights=[enWordEmbeddingModel.wv.vectors],
    trainable=False,
    name='word_embeddings_dekodera'
)(decoderInput)

decoderLSTMUnit = LSTM(LSTM_SIZE, return_sequences=True, return_state=True, name='jednostka_lstm_dekodera')
decoderOutputSequenceElement, _, _ = decoderLSTMUnit(decoderEmbeddingsModel,
                                                     initial_state=[hState, cState])

attention = dot([decoderOutputSequenceElement, encoderOutputSequence], axes=[2, 2], name='attention_I')
attentionLayer = Activation('softmax')(attention)

context = dot([attentionLayer, encoderOutputSequence], axes=[2,1], name='attention_II')
decoderCombinedContext = concatenate([context, decoderOutputSequenceElement], name='attention_III')

attentionDense = Dense(200, activation="tanh", name='attention_dense_layer')
attentionOutput = attentionDense(decoderCombinedContext)

decoderOutputLayer = Dense(len(enWordEmbeddingModel.wv.vectors), activation='softmax',
                           name='wartsta_wyjsciowa_dekodera')
decoderOutputSequenceElement = decoderOutputLayer(attentionOutput)

model = Model([encoderInput, decoderInput], decoderOutputSequenceElement)
plotModel(model, to_file='lstmatt_model.png')
model.compile(loss='categorical_crossentropy', optimizer='rmsprop', metrics=['acc'])
logging.info('Finish define model')

startTime = time.time()
history = model.fit([frWordsIndex, enWordsIndex],
                    enOneHotWords,
                    batch_size=128,
                    epochs=EPOCHS,
                    validation_split=0.1)
finishTime = time.time()

logging.info('Uczenie trwało %0.3f ms' % ((finishTime - startTime) * 1000.0))
logging.info('-------------END   SEQ2SEQ_LSTM--------------')

model.save('../models/seq2seq_lstm_att.model')
model.save_weights('../models/seq2seq_lstm_att.weights')

draw_data.drawLearningHistory(history)

decoderHStateInput = Input(shape=(LSTM_SIZE,))
decoderCStateInput = Input(shape=(LSTM_SIZE,))
decoderSequenceInput = Input(shape=(None,LSTM_SIZE))
decoderStatesInput = [decoderHStateInput, decoderCStateInput]
decoderOutputSequenceElement, hState, cState = decoderLSTMUnit(
    decoderEmbeddingsModel, initial_state=decoderStatesInput)
decoder_states = [hState, cState]

attention = dot([decoderOutputSequenceElement, decoderSequenceInput], axes=[2, 2])
attentionLayer = Activation('softmax')(attention)

context = dot([attentionLayer, decoderSequenceInput], axes=[2,1])
decoderCombinedContext = concatenate([context, decoderOutputSequenceElement])

attentionOutput = attentionDense(decoderCombinedContext)

decoderOutputSequenceElement = decoderOutputLayer(attentionOutput)

encoderModel = Model(encoderInput, encoder_states)
decoderModel = Model(
    [decoderInput] + decoderStatesInput + [decoderSequenceInput],
    [decoderOutputSequenceElement] + decoder_states)

def translate(inputText):
    inputTextTokens = nltk.word_tokenize(inputText)
    inputSequence = np.zeros((1, MAX_SENTENCE_LENGTH_ENCODER), dtype=np.float32)
    for i, token in enumerate(inputTextTokens):
        inputSequence[0, i] = frWordEmbeddingModel.wv.vocab[token].index
    [hState, cState, encoderOutputSequence] = encoderModel.predict(inputSequence)
    resultSequence = np.zeros((1, 1))
    resultSequence[0, 0] = enWordEmbeddingModel.wv.vocab['<BEGIN> '].index
    stopGenerating = False
    decodedSequence = ''
    currentLength = 0
    while not stopGenerating:
        currentLength = currentLength + 1
        outputOneHotDecodedWord, h, c = decoderModel.predict([resultSequence] + [hState, cState] + [encoderOutputSequence])
        wordIndex = np.argmax(outputOneHotDecodedWord)
        word = enWordEmbeddingModel.wv.index2word[wordIndex]
        decodedSequence = decodedSequence + word + ' '
        if currentLength > MAX_SENTENCE_LENGTH_DECODER or wordIndex == 1:
            stopGenerating = True
        [hState, cState] = [h, c]
        resultSequence = [wordIndex]
    return decodedSequence


print('sentence:' + 'vous êtes très attirant')
print('translation: ' + translate('vous êtes très attirant'))

print('sentence:' + 'tout est cassé')
print('translation: ' + translate('tout est cassé'))

print('sentence:' + 'Elle est très attirante')
print('translation: ' + translate('elle est très attirante'))

print('sentence:' + 'monte dans le siège arrière')
print('translation: ' + translate('monte dans le siège arrière'))

print('sentence:' + 'donnemoi ton adresse')
print('translation: ' + translate('donnemoi ton adresse'))
