import logging
import sys

sys.path.append('../common/')
import preprocessing

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

phrasesNumberLimit = 100000

epochs = 100
embeddingSize = 64

datasetName = '../dataset/sentence_pairs/fr.txt'

from gensim.models import Word2Vec

with open(datasetName) as f:
    phrases = f.readlines()[0:phrasesNumberLimit]
phrasesSplittedByWords = preprocessing.preprocessFrench(phrases)
del phrases

model = Word2Vec(phrasesSplittedByWords,
                 min_count=1,
                 size=embeddingSize,
                 window=3,
                 workers=16)
model.train(phrasesSplittedByWords,
            total_examples=len(phrasesSplittedByWords),
            epochs=epochs)
model.save('../models/word2vec_pairs_fr.model')
