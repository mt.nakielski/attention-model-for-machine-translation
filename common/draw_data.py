import matplotlib.pyplot as plt

def drawLearningHistory(history):
    plt.plot(history.history['acc'])
    plt.plot(history.history['val_acc'])
    plt.title('Dokładność modelu w przebiegu uczenia')
    plt.ylabel('dokładność [%]')
    plt.xlabel('przebieg nauki')
    plt.legend(['dane treningowe', 'dane testowe'], loc='lower right')
    plt.grid()
    plt.show()
    # summarize history for loss
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('Wartość funkcji kosztu w przebiegu uczenia')
    plt.ylabel('wartość funkcji kosztu')
    plt.xlabel('przebieg nauki')
    plt.legend(['dane treningowe', 'dane testowe'], loc='upper right')
    plt.grid()
    plt.show()

