import string

import nltk

CHARS_TO_REMOVE = set(r"""!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~""")
REMOVE_DIGITS = str.maketrans('', '', string.digits)

def preprocessEnglish(data):
    preprocessedData = map(lambda x: x.lower(), data)
    preprocessedData = map(lambda x: x.replace('\'', ''), preprocessedData)
    preprocessedData = map(lambda x: ''.join(ch for ch in x if ch not in CHARS_TO_REMOVE),
                                    preprocessedData)
    preprocessedData = map(lambda x: x.replace('\'', ''), preprocessedData)
    preprocessedData = map(lambda x: x.translate(REMOVE_DIGITS), preprocessedData)
    preprocessedData = map(lambda x: nltk.word_tokenize(x), preprocessedData)
    preprocessedData = list(map(lambda x: ['<BEGIN> '] + x + [' <END>'], preprocessedData))
    return preprocessedData

def preprocessFrench(data):
    preprocessedData = map(lambda x: x.lower(), data)
    preprocessedData = map(lambda x: x.replace('\'', ''), preprocessedData)
    preprocessedData = map(lambda x: ''.join(ch for ch in x if ch not in CHARS_TO_REMOVE),
                                    preprocessedData)
    preprocessedData = map(lambda x: x.replace('\'', ''), preprocessedData)
    preprocessedData = map(lambda x: x.translate(REMOVE_DIGITS), preprocessedData)
    preprocessedData = map(lambda x: nltk.word_tokenize(x), preprocessedData)
    preprocessedData = list(map(lambda x: x + ['<END>'], preprocessedData))
    return preprocessedData